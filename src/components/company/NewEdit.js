import React, { Component } from 'react';
import withRedux from '../../redux';
// import { Card } from "react-bootstrap";
// import { CardBody } from "reactstrap";
// import { Row, Form, Col, Button } from "react-bootstrap";
import axios from 'axios';
import { Formik, Form, Field, ErrorMessage, connect } from 'formik';
import {
  Card,
  CardBody,
  Row,
  Col,
  CardSubtitle,
  Button,
  CardHeader,
  FormGroup,
  Label,
} from 'reactstrap';

const NewEdit = (props) => {
  const [company, setCompany] = React.useState({ ...props.company });

  const changeHandler = (e) => {
    const newVal = { [e.target.name]: e.target.value };

    setCompany({
      ...company,
      ...newVal,
    });
  };

  const saveUpdateHandler = (e) => {
    e.preventDefault();

    const validate = (e) => {
      const {
        company_name,
        company_email,
        company_website,
        company_phone,
        company_address,
      } = e;

      console.log(e);

      const id = props.match.params.companyId;

      delete company.id;

      if (id) {
        axios
          .put(`/company/${id}`, company)
          .then((response) => {
            console.log(response);
            setTimeout(() => {
              props.history.push('/company');
            }, 2000);
          })
          .catch((error) => {
            console.log(error);
          });
      } else {
        axios
          .post(`/company`, company)
          .then((response) => {
            console.log(response);
            setTimeout(() => {
              props.history.push('/company');
            }, 2000);
          })
          .catch((error) => {
            console.log(error);
          });
      }
    };
  };

  const validate = (e) => {
    console.log('e', e);
    const errors = {};
    if (!e.company_name) {
      errors.company_name = 'Required';
    } else if (e.company_name.length > 15) {
      errors.company_name = 'Must be 15 characters or less';
    }

    if (!e.company_address) {
      errors.company_address = 'Required';
    } else if (e.company_address.length > 20) {
      errors.company_address = 'Must be 20 characters or less';
    }

    if (!e.company_phone) {
      errors.company_phone = 'Required';
    } else if (e.company_phone.length < 11) {
      errors.company_phone = 'Must be 11 characters or more';
    }

    if (!e.company_email) {
      errors.company_email = 'Required';
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(e.company_email)
    ) {
      errors.company_email = 'Invalid email address';
    }

    console.log('err', errors);
    return errors;
  };

  return (
    <Card>
      <CardHeader>Sign in to your account</CardHeader>
      <CardBody>
        <CardSubtitle></CardSubtitle>
        {/* <hr /> */}

        <Formik
          initialValues={
            props.company
              ? { ...props.company }
              : {
                  company_name: '',
                  company_email: '',
                  company_website: '',
                  company_address: '',
                  company_phone: '',
                }
          }
          validate={validate}
          onSubmit={saveUpdateHandler}
        >
          <Form>
            <FormGroup>
              <Label>Company Name</Label>
              <Field
                type='text'
                className='form-control'
                value={company.company_name}
                name='company_name'
              />
              <ErrorMessage
                name='company_name'
                component='small'
                className='text-danger'
              />
            </FormGroup>

            <FormGroup>
              <Label>Company Email</Label>
              <Field
                type='text'
                className='form-control'
                value={company.company_email}
                name='company_email'
              />
              <ErrorMessage
                name='company_email'
                component='small'
                className='text-danger'
              />
            </FormGroup>
            <FormGroup>
              <Label>Company Website</Label>
              <Field
                type='text'
                className='form-control'
                value={company.company_website}
                name='company_website'
              />
              <ErrorMessage
                name='company_website'
                component='small'
                className='text-danger'
              />
            </FormGroup>
            <FormGroup>
              <Label>Company Address</Label>
              <Field
                type='text'
                className='form-control'
                value={company.company_address}
                name='company_address'
              />
              <ErrorMessage
                name='company_address'
                component='small'
                className='text-danger'
              />
            </FormGroup>
            <FormGroup>
              <Label>Company Phone</Label>
              <Field
                type='text'
                className='form-control'
                value={company.company_phone}
                name='company_phone'
              />
              <ErrorMessage
                name='company_phone'
                component='small'
                className='text-danger'
              />
            </FormGroup>

            <Row>
              <div className='col'>
                <p>
                  <small>
                    <a href='/' className='text-secondary'>
                      Forgot password?
                    </a>
                    <br />
                  </small>
                  <a href='/'>Sign up for a new account</a>
                </p>
              </div>
              <div className='col-auto'>
                <Button variant='primary' type='submit'>
                  Save
                </Button>
              </div>
            </Row>
          </Form>
        </Formik>
      </CardBody>
    </Card>
  );
};

export default withRedux(NewEdit);
