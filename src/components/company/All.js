import React from "react";
import axios from "axios";

import { NavLink, Route } from "react-router-dom";
import { Row, Form, Col, Button, Pagination } from "react-bootstrap";
import {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Input,
  Table,
} from "reactstrap";
import withRedux from "../../redux";
import Pages from "../common/Pages";
import img1 from "../../assets/images/users/1.jpg";

const initState = [];

const All = (props) => {
  const [companies, setCompanies] = React.useState([]);
  const [pages, setPages] = React.useState({});

  // const company = {
  //   id: 1,
  //   company_name: "Acme",
  // };

  const initGrid = () => {
    const current = pages.current || 1;

    axios
      .get(`${process.env.REACT_APP_BASE_URL}/company?page=${current}`)
      .then(({ data: { data } }) => {
        const { count, limit } = data;
        setCompanies(data.rows);
        // getCompanies(data.rows);
        setPages({ count, limit, current });
      });
  };

  React.useEffect(() => {
    initGrid();
  }, [pages.current]);

  const handleDelete = async (obj) => {
    axios
      .delete(`/company/${obj.id}`)
      .then((response) => {
        initGrid();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // const formSubmitHandler = (form) => {
  //   const req = {
  //     Add: {
  //       method: "post",
  //       url: `/company`,
  //     },
  //     Edit: {
  //       method: "put",
  //       url: `/company/${form.id}`,
  //     },
  //   };

  //   delete form["id"];

  //   axios[req[tasks.operation]["method"]](req[tasks.operation]["url"], form)
  //     .then((response) => {
  //       initGrid();
  //     })
  //     .catch((err) => {});
  // };

  // const closeHandler = () => {
  //   console.log("closed");
  //   setAddNew(false);
  // };

  const onClickHandler = (current) => {
    setPages({ ...pages, current });
  };

  return (
    /*--------------------------------------------------------------------------------*/
    /* Used In Dashboard-4 [General]                                                  */
    /*--------------------------------------------------------------------------------*/

    <Card>
      <CardBody>
        <div className="d-flex align-items-center">
          <div>
            <CardTitle>Companies of the Month</CardTitle>
            <CardSubtitle>Overview of Latest Month</CardSubtitle>
          </div>
          <div className="ml-auto d-flex no-block align-items-center">
            <div className="ml-auto d-flex">
              <NavLink
                onClick={() => props.editCompany({company_name: '', company_email: '', company_website: '', company_address: '', company_phone: ''})}
                className="button is-link"
                activeClassName="active"
                exact
                to="/company/new"
              >
                Add Company
              </NavLink>

              {/* <AddNew
                    {...tasks}
                    onFormSubmit={(form) => formSubmitHandler(form)}
                    show={addNew}
                    onClose={closeHandler}
                  /> */}
            </div>
            {/* <div className="dl">
                <Input type="select" className="custom-select">
                  <option value="0">Monthly</option>
                  <option value="1">Daily</option>
                  <option value="2">Weekly</option>
                  <option value="3">Yearly</option>
                </Input>
              </div> */}
          </div>
        </div>
        <Table className="no-wrap v-middle" responsive>
          <thead>
            <tr className="border-0">
              <th className="border-0">Company Name</th>
              <th className="border-0">Company Email</th>

              <th className="border-0">Company Address</th>
              <th className="border-0">Company website</th>
              <th className="border-0">Company Phone</th>
              <th className="border-0">Actions</th>
            </tr>
          </thead>
          <tbody>
            {companies.length ? (
              companies.map((company) => {
                return (
                  <tr key={company.id}>
                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_name}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_email}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_address}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_website}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_phone}
                          </h5>
                        </div>
                      </div>
                    </td>
                    <td>
                      <NavLink
                        onClick={() => props.editCompany(company)}
                        className="item"
                        className="button is-success is-outlined"
                        exact
                        to={`/company/${company.id}/edit`}
                      >
                        <span>Edit</span>
                        <span className="icon is-small">
                          <i className="fas fa-edit"></i>
                        </span>
                      </NavLink>
                    </td>
                    <td>
                      <button
                        className="button is-danger is-outlined"
                        onClick={() => handleDelete(company)}
                      >
                        <span>Delete</span>
                        <span className="icon is-small">
                          <i className="fas fa-times"></i>
                        </span>
                      </button>
                    </td>
                  </tr>
                );
              })
            ) : (
              <tr>
                <td colSpan="7">No Companies Found</td>
              </tr>
            )}
          </tbody>
          <tfoot>
            <tr>
              <td colSpan="7">
                <Pagination size={"sm"}>
                  <Pages
                    {...pages}
                    gotoPage={(current) => onClickHandler(current)}
                  />
                </Pagination>
              </td>
            </tr>
          </tfoot>
        </Table>
      </CardBody>
    </Card>
  );
};

export default withRedux(All);
