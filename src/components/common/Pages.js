import React from "react";
import { Pagination } from "react-bootstrap";

const Pages = ({ count, limit, gotoPage, current }) => {
  const pages = [];

  for (let number = 1; number <= Math.ceil(count / limit); number++) {
    pages.push(
      <Pagination.Item
        key={number}
        active={number === current} 
        onClick={() => gotoPage(number)}
      >
        {number}
      </Pagination.Item>
    );
  }

  return pages;
};

export default Pages;
