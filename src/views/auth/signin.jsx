import React from 'react';
import {
  Card,
  CardBody,
  Row,
  Col,
  CardSubtitle,
  Button,
  CardHeader,
  FormGroup,
  Label,
} from 'reactstrap';

import { Formik, Form, Field, ErrorMessage } from 'formik';

import withRedux from '../../redux';
import axios from 'axios';

const Signin = (props) => {
  const handleLogin = (values, { setSubmitting }) => {
    const { email, password } = values;

    axios
      .post('auth', {
        email,
        password,
      })
      .then((response) => {
        const {
          status,
          data: { token },
        } = response;

        if (status === 200) {
          localStorage.setItem(
            `${process.env.REACT_APP_APP_NAME}-token`,
            token
          );
          props.changeLoggedInFlag(true);
          props.login(email, password);
        } else {
          console.log('Invalid details: ', status);
        }
      })
      .catch((error) => {
        console.log('here', error.response);
      });

    setSubmitting(true);

    props.changeLoggedInFlag(false);
  };

  const handleValidations = (values) => {
    const errors = {};
    if (!values.email) {
      errors.email = '* Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
      errors.email = '* Invalid username';
    }
    if (!values.password) {
      errors.password = '* Required';
    }
    return errors;
  };

  const token =
    localStorage.getItem(`${process.env.REACT_APP_APP_NAME}-token`) || null;

  React.useEffect(() => {
    if (token) {
      axios
        .post('/users/me', {
          token,
        })
        .then((res) => {})
        .catch((err) => {});
    }
  }, [token]);

  return (
    <Row className=' '>
      <Col sm='2' md='3' />
      <Col sm='8' md='6'>
        <br />
        <br />
        <br />
        <br />

        <Card>
          <CardHeader>Sign in to your account</CardHeader>
          <CardBody>
            <CardSubtitle></CardSubtitle>
            {/* <hr /> */}

            <Formik
              initialValues={{ email: '', password: '' }}
              validate={handleValidations}
              onSubmit={handleLogin}
            >
              {({ isSubmitting }) => (
                <Form>
                  <FormGroup>
                    <Label>Username</Label>
                    <Field
                      type='email'
                      className='form-control'
                      placeholder='someone@example.com'
                      name='email'
                    />
                    <ErrorMessage
                      name='email'
                      component='small'
                      className='text-danger'
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for='password'>Password</Label>
                    <Field
                      type='password'
                      className='form-control'
                      name='password'
                      placeholder='********'
                    />
                    <ErrorMessage
                      name='password'
                      component='small'
                      className='text-danger'
                    />
                  </FormGroup>
                  <Row>
                    <div className='col'>
                      <p>
                        <small>
                          <a href='/' className='text-secondary'>
                            Forgot password?
                          </a>
                          <br />
                        </small>
                        <a href='/'>Sign up for a new account</a>
                      </p>
                    </div>
                    <div className='col-auto'>
                      <Button
                        className='btn btn-success'
                        type='submit'
                        disabled={isSubmitting}
                      >
                        Sign in
                      </Button>
                    </div>
                  </Row>
                </Form>
              )}
            </Formik>
          </CardBody>
        </Card>
      </Col>
    </Row>
  );
};

export default withRedux(Signin);
