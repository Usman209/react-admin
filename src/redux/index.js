import { connect } from "react-redux";
import actions from "./actions";

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = actions;

export default connect(mapStateToProps, mapDispatchToProps);
