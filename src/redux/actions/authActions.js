import { CHANGE_LOGGED_IN_FLAG, SET_CURRENT_USER } from "../constants";
import { setError } from "./mainActions";

export const changeLoggedInFlag = (isLoggedIn) => {
  return {
    type: CHANGE_LOGGED_IN_FLAG,
    isLoggedIn,
  };
};

export const setCurrentUser = (user) => {
  return {
    type: SET_CURRENT_USER,
    currentUser: user,
  };
};

export const login = (username = "", password = "") => {
  const user = { username, password };

  return async (dispatch) => {
    try {
      dispatch(setCurrentUser(user));
    } catch (err) {
      console.log("Error in getUserById", err.message);
      dispatch(setError(err.message));
    }
  };
};
