import * as authActions from "./authActions";
import * as mainActions from "./mainActions";
import * as companyActions from "./companyActions";

const rootActions = {
  ...authActions,
  ...mainActions,
  ...companyActions,
};

export default rootActions;
